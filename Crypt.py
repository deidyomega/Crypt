# -*- coding: utf-8 -*-
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Hash import SHA256
import codecs


class Crypt:
    def __init__(self, key):
        key = codecs.encode(key, 'utf-8')
        self.key = SHA256.new(key).digest()
        self.mode = AES.MODE_CFB

    def encrypt(self, plaintext):
        iv = Random.new().read(16)
        aes = AES.new(self.key, self.mode, IV=iv)
        ciphertext = aes.encrypt(plaintext)
        cipherIV = b"".join([ciphertext,iv])
        return codecs.encode(cipherIV, 'hex_codec')

    def decrypt(self, cipherIV):
        cipherIV = codecs.decode(cipherIV, 'hex_codec')
        ciphertext = cipherIV[:-16]
        iv = cipherIV[-16:]
        aes = AES.new(self.key, self.mode, IV=iv)
        plaintext = aes.decrypt(ciphertext)
        return codecs.encode(plaintext, "utf-8")

# unit test
if __name__ == '__main__':
    crypt = Crypt("password")
    ciphertext= crypt.encrypt("If you can read this, the class is working")
    plaintext = crypt.decrypt(ciphertext)
    print(plaintext)
